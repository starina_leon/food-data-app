import {Component, Input} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store/root.reducer';
import {OpenFoodItemEditDialog} from '../../../store/dashboard/actions';
import {FoodItem} from '../../../store/dashboard/reducer';

@Component({
	selector: 'app-dashboard-group-container',
	template:
		'<app-dashboard-group [meal]="meal" [group]="group" (editClicked)="clickOnEdit($event)"></app-dashboard-group>'
})
export class DashboardGroupContainer {
	@Input() meal: FoodItem[] = [];
	@Input() group: string;
	constructor(private store: Store<AppState>) {}

	clickOnEdit(event: FoodItem) {
		this.store.dispatch(new OpenFoodItemEditDialog({food: event, group: this.group}));
	}
}
