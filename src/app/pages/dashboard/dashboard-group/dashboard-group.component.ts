import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';
import {FoodItem} from '../../../store/dashboard/reducer';

@Component({
	selector: 'app-dashboard-group',
	templateUrl: './dashboard-group.component.html',
	styleUrls: ['./dashboard-group.component.scss']
})
export class DashboardGroupComponent implements OnInit {
	@Input() meal: FoodItem[] = [];
	@Input() group: string;
	@Output() editClicked = new EventEmitter<FoodItem>();

	constructor(private router: Router) {}

	ngOnInit(): void {}

	searchFood() {
		localStorage.setItem('group', this.group);
		this.router.navigate(['/search']);
	}

	openDialog(food: FoodItem) {
		this.editClicked.emit(food);
	}
}
