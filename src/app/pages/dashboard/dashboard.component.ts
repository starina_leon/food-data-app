import {Component, Input, OnInit, Output} from '@angular/core';
import {TYPES_COLORS} from '../../shared/constants';
import {DashboardDayState} from '../../store/dashboard/reducer';
import {Progress} from '../../shared/interfaces/food-data.interface';

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
	@Input() data: DashboardDayState;
	@Input() progress: Progress;
	@Input() weights;

	TYPES_COLORS = TYPES_COLORS;

	constructor() {}

	ngOnInit(): void {
		localStorage.removeItem('group');
	}
}
