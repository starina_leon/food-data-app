import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../../store/root.reducer';
import {ClearSearchResults} from '../../store/search/actions';
import {Observable} from 'rxjs';
import {
	getDashboardDateMeals,
	getDashboardDateSums,
	getDashboardProgress,
	getDashboardWeights
} from '../../store/dashboard/selectors';

import {DashboardDayState} from '../../store/dashboard/reducer';

@Component({
	selector: 'app-dashboard-container',
	template:
		'<app-dashboard [weights]="foodWeights$ | async" [data]="data$ | async" [progress]="nutrientWeights$ | async"></app-dashboard>'
})
export class DashboardContainer implements OnInit {
	nutrientWeights$: Observable<any>;
	foodWeights$: Observable<any>;

	data$: Observable<DashboardDayState> = this.store.select(getDashboardDateMeals);

	constructor(private store: Store<AppState>) {
		this.foodWeights$ = this.store.select(getDashboardWeights);
		this.nutrientWeights$ = this.store.select(getDashboardProgress);
	}

	ngOnInit() {
		this.store.dispatch(new ClearSearchResults());
	}
}
