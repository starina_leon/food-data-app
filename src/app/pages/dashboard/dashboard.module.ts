import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DashboardComponent} from './dashboard.component';
import {MatCardModule} from '@angular/material/card';
import {DashboardRoutingModule} from './dashboard.routing';
import {DashboardGroupComponent} from './dashboard-group/dashboard-group.component';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {DashboardContainer} from './dashboard.container';
import {ProgressComponent} from './progress/progress.component';
import {TimePickerComponent} from './time-picker/time-picker.component';
import {MatTooltipModule} from '@angular/material/tooltip';
import {DialogPageComponent} from './dialog-page/dialog-page.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDialogModule} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';
import {DialogPageContainer} from './dialog-page/dialog-page.container';
import {DashboardGroupContainer} from './dashboard-group/dashboard-group.container';
import {TimePickerContainer} from './time-picker/time-picker.container';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		MatCardModule,
		DashboardRoutingModule,
		MatButtonModule,
		MatIconModule,
		MatTooltipModule,
		MatFormFieldModule,
		MatDialogModule,
		MatInputModule
	],
	declarations: [
		DashboardComponent,
		DashboardGroupComponent,
		DashboardGroupContainer,
		DashboardContainer,
		ProgressComponent,
		TimePickerComponent,
		TimePickerContainer,
		DialogPageComponent,
		DialogPageContainer
	],
	exports: [DashboardComponent]
})
export class DashboardModule {}
