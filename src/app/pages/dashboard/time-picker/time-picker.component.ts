import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import * as moment from 'moment';
import {DateService} from '../../../shared/services/date.service';

@Component({
	selector: 'app-time-picker',
	templateUrl: './time-picker.component.html',
	styleUrls: ['./time-picker.component.scss']
})
export class TimePickerComponent implements OnInit {
	public formattedDate: string;
	public isNextDayAvailable = false;

	constructor(private dateService: DateService) {
		this.dateService.date.subscribe((value) => {
			this.formattedDate = value.format('DD MMM, YYYY');
			this.isNextDayAvailable = value < moment().startOf('day');
		});
	}

	ngOnInit(): void {}

	toNextDate() {
		this.dateService.toNextDate();
	}

	toPreviousDate() {
		this.dateService.toPreviousDate();
	}
}
