import {Component} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store/root.reducer';
import {DateService} from '../../../shared/services/date.service';
import {SelectDate} from '../../../store/dashboard/actions';
import {DATE_FORMAT_URL} from '../../../shared/constants';
import {delay} from 'rxjs/operators';

@Component({
	selector: 'app-time-picker-container',
	template: '<app-time-picker></app-time-picker>'
})
export class TimePickerContainer {
	constructor(private store: Store<AppState>, private dateService: DateService) {
		dateService.date.subscribe((date) => {
			this.store.dispatch(new SelectDate({date: date.format(DATE_FORMAT_URL)}));
		});
	}
}
