import {Component, Inject, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store/root.reducer';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {DialogData} from '../../../shared/interfaces/food-data.interface';
import {DeleteFoodItem} from '../../../store/dashboard/actions';

@Component({
	selector: 'app-dialog-page-container',
	template:
		'<app-dialog-page  (itemDeleted)="deleteItem()" (dialogClosed)="onNoClick()" (weightSettled)="setWeight($event)" [hideRemoveButton]="hideRemoveButton"></app-dialog-page>'
})
export class DialogPageContainer implements OnInit {
	hideRemoveButton: boolean;

	constructor(
		private store: Store<AppState>,
		public dialogRef: MatDialogRef<any>,
		@Inject(MAT_DIALOG_DATA) public data: DialogData
	) {}

	ngOnInit() {
		this.hideRemoveButton = this.data.hideRemoveButton;
	}

	setWeight(event) {
		this.dialogRef.close({weight: event});
	}

	deleteItem() {
		this.store.dispatch(new DeleteFoodItem({id: this.data.food.id, group: this.data.group}));
		this.dialogRef.close();
	}

	onNoClick() {
		this.dialogRef.close();
	}
}
