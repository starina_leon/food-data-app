import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
	selector: 'app-dialog-page',
	templateUrl: './dialog-page.component.html',
	styleUrls: ['./dialog-page.component.css']
})
export class DialogPageComponent implements OnInit {
	@Input() hideRemoveButton: boolean;
	@Output() itemDeleted = new EventEmitter();
	@Output() dialogClosed = new EventEmitter();
	@Output() weightSettled = new EventEmitter();
	weight: number;

	constructor() {}

	ngOnInit(): void {}

	deleteItem() {
		this.itemDeleted.emit();
	}

	onNoClick() {
		this.dialogClosed.emit();
	}

	setWeight(weight: number) {
		this.weightSettled.emit(weight);
	}
}
