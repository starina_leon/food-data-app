import {Component} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../../store/root.reducer';
import {ChangeSearchQuery} from '../../store/search/actions';
import {Observable} from 'rxjs';
import {getSearchResults} from '../../store/search/selectors';
import {AddFromSearchClicked} from '../../store/dashboard/actions';

@Component({
	selector: 'app-food-search-container',
	template:
		'<app-food-search (changeSearchQuery)="changeSearchQuery($event)" [searchResult]="searchResults$ | async" (foodAdded)="addFoodItem($event)"></app-food-search>'
})
export class FoodSearchContainer {
	public searchResults$: Observable<any>;

	constructor(private store: Store<AppState>) {
		this.searchResults$ = this.store.select(getSearchResults);
	}

	changeSearchQuery(query: string) {
		this.store.dispatch(new ChangeSearchQuery(query));
	}

	addFoodItem(food) {
		this.store.dispatch(new AddFromSearchClicked({food}));
	}
}
