import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FoodSearchComponent} from './food-search.component';
import {SearchRoutingModule} from './search.routing';
import {SearchResultComponent} from './search-result/search-result.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatListModule} from '@angular/material/list';
import {FoodSearchContainer} from './food-search.container';

@NgModule({
	imports: [
		FormsModule,
		ReactiveFormsModule,
		CommonModule,
		SearchRoutingModule,
		MatFormFieldModule,
		MatInputModule,
		MatIconModule,
		MatButtonModule,
		MatListModule
	],
	declarations: [FoodSearchComponent, SearchResultComponent, FoodSearchContainer],
	exports: [FoodSearchComponent]
})
export class SearchModule {}
