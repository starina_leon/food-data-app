import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {FoodSearchContainer} from './food-search.container';

const routes: Routes = [
	{
		path: '',
		component: FoodSearchContainer
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class SearchRoutingModule {}
