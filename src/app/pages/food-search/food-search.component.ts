import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {debounceTime, filter} from 'rxjs/operators';
import {Store} from '@ngrx/store';
import {AppState} from '../../store/root.reducer';

@Component({
	selector: 'app-food-search',
	templateUrl: './food-search.component.html',
	styleUrls: ['./food-search.component.scss']
})
export class FoodSearchComponent implements OnInit {
	@Output() changeSearchQuery = new EventEmitter();
	@Output() foodAdded = new EventEmitter<any>();
	@Input() searchResult;
	form: FormGroup;

	constructor(private store: Store<AppState>) {
		this.createSearchForm();
	}

	ngOnInit(): void {
		this.onChanges();
	}

	private createSearchForm() {
		this.form = new FormGroup({
			searchField: new FormControl('')
		});
	}

	onChanges() {
		this.form.valueChanges
			.pipe(
				debounceTime(500),
				filter((value) => !!value.searchField)
			)
			.subscribe((val) => {
				this.getSearchQuery();
			});
	}

	clearSearch() {
		this.searchResult = null;
		this.form.reset();
	}

	getSearchQuery() {
		this.changeSearchQuery.emit(this.form.get('searchField').value);
	}
}
