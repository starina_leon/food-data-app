import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {getURL} from '../../../shared/helper';

@Component({
	selector: 'app-search-result',
	templateUrl: './search-result.component.html',
	styleUrls: ['./search-result.component.scss']
})
export class SearchResultComponent implements OnInit {
	@Input() food;
	@Output() foodAdded = new EventEmitter<any>();

	constructor() {}

	ngOnInit(): void {}

	openDetails() {
		window.open(getURL(this.food));
	}
}
