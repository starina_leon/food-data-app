import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SettingsComponent} from './settings.component';
import {SettingsRoutingModule} from './settings.routing';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {SettingsContainer} from './settings.container';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		SettingsRoutingModule,
		MatFormFieldModule,
		MatInputModule,
		MatButtonModule
	],
	declarations: [SettingsComponent, SettingsContainer],
	exports: [SettingsComponent]
})
export class SettingsModule {}
