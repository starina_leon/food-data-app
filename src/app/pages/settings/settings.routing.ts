import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {SettingsContainer} from './settings.container';

const routes: Routes = [
	{
		path: '',
		component: SettingsContainer
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class SettingsRoutingModule {}
