import {Component} from '@angular/core';

@Component({
	selector: 'app-settings-container',
	template: '<app-settings></app-settings>'
})
export class SettingsContainer {}
