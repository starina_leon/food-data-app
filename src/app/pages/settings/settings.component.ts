import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
	selector: 'app-settings',
	templateUrl: './settings.component.html',
	styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
	form: FormGroup;

	constructor() {
		this.createSettingsForm();
	}

	ngOnInit(): void {}

	private createSettingsForm() {
		this.form = new FormGroup({
			calories: new FormControl(''),
			carbs: new FormControl(''),
			fats: new FormControl(''),
			proteins: new FormControl('')
		});
	}

	saveSettings() {
		localStorage.setItem('settings', JSON.stringify(this.form.value));
	}
}
