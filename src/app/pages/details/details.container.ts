import {Component, OnDestroy} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../../store/root.reducer';
import {ClearDetails, LoadDetails} from '../../store/details/actions';
import {Observable} from 'rxjs';
import {getDetailsInfo} from '../../store/details/selectors';

@Component({
	selector: 'app-details-container',
	template: '<app-details (receivedId)="loadDetails($event)" [detailsInfo]="detailsInfo$ | async"></app-details>'
})
export class DetailsContainer implements OnDestroy {
	detailsInfo$: Observable<any>;

	constructor(private store: Store<AppState>) {
		this.detailsInfo$ = this.store.select(getDetailsInfo);
	}

	public loadDetails(id: string): void {
		this.store.dispatch(new LoadDetails({id}));
	}

	ngOnDestroy() {
		this.store.dispatch(new ClearDetails());
	}
}
