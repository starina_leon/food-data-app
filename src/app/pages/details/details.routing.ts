import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DetailsContainer} from './details.container';

const routes: Routes = [
	{
		path: ':id',
		component: DetailsContainer
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class DetailsRoutingModule {}
