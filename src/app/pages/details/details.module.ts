import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DetailsComponent} from './details.component';
import {DetailsContainer} from './details.container';
import {DetailsRoutingModule} from './details.routing';
import {MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';

@NgModule({
	imports: [CommonModule, DetailsRoutingModule, MatTableModule, MatIconModule, MatButtonModule],
	declarations: [DetailsComponent, DetailsContainer],
	exports: [DetailsContainer]
})
export class DetailsModule {}
