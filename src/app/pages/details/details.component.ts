import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {getURL} from '../../shared/helper';

@Component({
	selector: 'app-details',
	templateUrl: './details.component.html',
	styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit, OnChanges {
	columnsName = ['Proximates', 'Carbohydrates'];
	@Output() receivedId = new EventEmitter<string>();
	@Input() detailsInfo;
	displayedColumns: string[] = ['name', 'average amount', 'unit'];
	foodNutrients: [];

	constructor(private activatedRoute: ActivatedRoute) {}

	ngOnInit(): void {
		this.activatedRoute.params.subscribe(({id}) => {
			this.receivedId.emit(id);
		});
	}

	ngOnChanges() {
		this.foodNutrients = this.detailsInfo.foodNutrients;
	}

	openDetails() {
		window.open(getURL(this.detailsInfo));
	}
}
