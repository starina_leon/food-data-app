import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {API_KEY} from './shared/constants';

@Injectable({
	providedIn: 'root'
})
export class FetchDataService {
	constructor(private http: HttpClient) {}
	private API_KEY = API_KEY;

	getData(searchQuery) {
		return this.http.get(
			`https://api.nal.usda.gov/fdc/v1/foods/search?api_key=${this.API_KEY}&query=${searchQuery}&dataType=Foundation`
		);
	}

	getDetails(id: string) {
		return this.http.get(`https://api.nal.usda.gov/fdc/v1/food/${id}?api_key=${this.API_KEY}`);
	}
}
