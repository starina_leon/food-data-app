import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
	{
		path: '',
		redirectTo: 'dashboard',
		pathMatch: 'full'
	},
	{
		path: 'dashboard',
		loadChildren: () => import('src/app/pages/dashboard/dashboard.module').then((m) => m.DashboardModule)
	},
	{
		path: 'search',
		loadChildren: () => import('src/app/pages/food-search/search.module').then((m) => m.SearchModule)
	},
	{
		path: 'settings',
		loadChildren: () => import('src/app/pages/settings/settings.module').then((m) => m.SettingsModule)
	},
	{
		path: 'details',
		loadChildren: () => import('src/app/pages/details/details.module').then((m) => m.DetailsModule)
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule {}
