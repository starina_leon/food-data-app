import {AppState} from '../root.reducer';
import {createSelector} from '@ngrx/store';

export const getSettingsState = (state: AppState) => state.settings;

export const getCalories = createSelector(
	getSettingsState,
	(state) => state.calories
);

export const getCarbs = createSelector(
	getSettingsState,
	(state) => state.carbs
);

export const getFats = createSelector(
	getSettingsState,
	(state) => state.fats
);

export const getProteins = createSelector(
	getSettingsState,
	(state) => state.proteins
);
