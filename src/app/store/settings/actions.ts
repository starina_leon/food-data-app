import {Action} from '@ngrx/store';

const moduleName = 'Settings';

export const SETTINGS_ACTION_TYPES = {
	CALORIES_CHANGED: `[${moduleName}] Calories Changed`,
	CARBS_CHANGED: `[${moduleName}] Carbs Changed`,
	FATS_CHANGED: `[${moduleName}] Fats Changed`,
	PROTEINS_CHANGED: `[${moduleName}] Proteins Changed`
};

export class ChangeCalories implements Action {
	readonly type = SETTINGS_ACTION_TYPES.CALORIES_CHANGED;

	constructor(public payload: number) {}
}

export class ChangeCarbs implements Action {
	readonly type = SETTINGS_ACTION_TYPES.CARBS_CHANGED;

	constructor(public payload: number) {}
}

export class ChangeFats implements Action {
	readonly type = SETTINGS_ACTION_TYPES.FATS_CHANGED;

	constructor(public payload: number) {}
}

export class ChangeProteins implements Action {
	readonly type = SETTINGS_ACTION_TYPES.PROTEINS_CHANGED;

	constructor(public payload: number) {}
}
