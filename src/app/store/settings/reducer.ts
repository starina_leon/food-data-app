import {SETTINGS_ACTION_TYPES} from './actions';

export interface SettingsState {
	calories: number;
	carbs: number;
	fats: number;
	proteins: number;
}

const data = JSON.parse(localStorage.getItem('settings'));

const initialSettingsState: SettingsState = data || {
	calories: 0,
	carbs: 0,
	fats: 0,
	proteins: 0
};

export function settingsReducer(state = initialSettingsState, action) {
	switch (action.type) {
		case SETTINGS_ACTION_TYPES.CALORIES_CHANGED:
			return {
				...state,
				calories: action.payload
			};
		case SETTINGS_ACTION_TYPES.CARBS_CHANGED:
			return {
				...state,
				carbs: action.payload
			};
		case SETTINGS_ACTION_TYPES.FATS_CHANGED:
			return {
				...state,
				fats: action.payload
			};
		case SETTINGS_ACTION_TYPES.PROTEINS_CHANGED:
			return {
				...state,
				proteins: action.payload
			};
		default:
			return state;
	}
}
