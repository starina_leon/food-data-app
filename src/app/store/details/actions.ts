import {Action} from '@ngrx/store';

const moduleName = 'Details';

export const DETAILS_ACTION_TYPES = {
	LOAD_DETAILS_RESULTS: {
		REQUESTED: `[${moduleName}] Search results requested`,
		SUCCEEDED: `[${moduleName}] Search results succeeded`,
		FAILED: `[${moduleName}] Search results failed`
	},
	CLEAR_DETAILS: `[${moduleName}] Clear Details`
};

export class LoadDetails implements Action {
	readonly type = DETAILS_ACTION_TYPES.LOAD_DETAILS_RESULTS.REQUESTED;

	constructor(public payload: {id: string}) {}
}

export class ClearDetails implements Action {
	readonly type = DETAILS_ACTION_TYPES.CLEAR_DETAILS;
}
