import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Action, Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {DETAILS_ACTION_TYPES, LoadDetails} from './actions';
import {map, switchMap} from 'rxjs/operators';
import {AppState} from '../root.reducer';
import {FetchDataService} from '../../fetch-data.service';

@Injectable()
export class DetailsEffects {
	constructor(private action$: Actions, private store: Store<AppState>, private fetchService: FetchDataService) {}

	@Effect()
	loadDetails$: Observable<Action> = this.action$.pipe(
		ofType(DETAILS_ACTION_TYPES.LOAD_DETAILS_RESULTS.REQUESTED),
		switchMap((action: LoadDetails) =>
			this.fetchService.getDetails(action.payload.id).pipe(
				map((details: any) => ({
					type: DETAILS_ACTION_TYPES.LOAD_DETAILS_RESULTS.SUCCEEDED,
					payload: details
				}))
			)
		)
	);
}
