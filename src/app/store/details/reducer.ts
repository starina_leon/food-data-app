import {DETAILS_ACTION_TYPES} from './actions';

export interface DetailsState {
	info: {};
}

const initialDetailsState: DetailsState = {
	info: {}
};

export function detailsReducer(state = initialDetailsState, action) {
	switch (action.type) {
		case DETAILS_ACTION_TYPES.LOAD_DETAILS_RESULTS.SUCCEEDED:
			return {
				...state,
				info: action.payload
			};
		case DETAILS_ACTION_TYPES.CLEAR_DETAILS:
			return {
				...state,
				info: initialDetailsState.info
			};
		default:
			return state;
	}
}
