import {AppState} from '../root.reducer';
import {createSelector} from '@ngrx/store';

export const getDetailsState = (state: AppState) => state.details;

export const getDetailsInfo = createSelector(
	getDetailsState,
	(state) => state.info
);
