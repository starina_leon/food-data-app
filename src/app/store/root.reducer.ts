import {settingsReducer, SettingsState} from './settings/reducer';
import {searchReducer, SearchState} from './search/reducer';
import {detailsReducer, DetailsState} from './details/reducer';
import {dashboardReducer, DashboardState} from './dashboard/reducer';

export const reducers = {
	settings: settingsReducer,
	search: searchReducer,
	details: detailsReducer,
	dashboard: dashboardReducer
};

export interface AppState {
	settings: SettingsState;
	search: SearchState;
	details: DetailsState;
	dashboard: DashboardState;
}
