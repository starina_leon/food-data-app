import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Action, Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {SEARCH_ACTION_TYPES} from './actions';
import {map, mapTo, switchMap, withLatestFrom} from 'rxjs/operators';
import {FetchDataService} from '../../fetch-data.service';
import {getSearchQuery} from './selectors';
import {AppState} from '../root.reducer';

@Injectable()
export class SearchEffects {
	constructor(private action$: Actions, private store: Store<AppState>, private fetchService: FetchDataService) {}

	@Effect()
	loadSearchResults$: Observable<Action> = this.action$.pipe(
		ofType(SEARCH_ACTION_TYPES.LOAD_SEARCH_RESULTS.REQUESTED),
		withLatestFrom(this.store.select(getSearchQuery)),
		switchMap(([action, searchQuery]) =>
			this.fetchService.getData(searchQuery).pipe(
				map((searchResults: any) => ({
					type: SEARCH_ACTION_TYPES.LOAD_SEARCH_RESULTS.SUCCEEDED,
					payload: searchResults.foods
				}))
			)
		)
	);

	@Effect()
	triggerLoad$: Observable<Action> = this.action$.pipe(
		ofType(SEARCH_ACTION_TYPES.SEARCH_QUERY_CHANGED),
		mapTo({type: SEARCH_ACTION_TYPES.LOAD_SEARCH_RESULTS.REQUESTED})
	);
}
