import {AppState} from '../root.reducer';
import {createSelector} from '@ngrx/store';

export const getSearchState = (state: AppState) => state.search;

export const getSearchQuery = createSelector(
	getSearchState,
	(state) => state.searchQuery
);

export const getSearchResults = createSelector(
	getSearchState,
	(state) => state.searchResult
);
