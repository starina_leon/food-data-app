import {Action} from '@ngrx/store';

const moduleName = 'Search';

export const SEARCH_ACTION_TYPES = {
	SEARCH_QUERY_CHANGED: `[${moduleName}] Search Query Changed`,
	LOAD_SEARCH_RESULTS: {
		REQUESTED: `[${moduleName}] Search results requested`,
		SUCCEEDED: `[${moduleName}] Search results succeeded`,
		FAILED: `[${moduleName}] Search results failed`
	},
	CLEAR_SEARCH_RESULTS: `[${moduleName}] Search results cleared`
};

export class ChangeSearchQuery implements Action {
	readonly type = SEARCH_ACTION_TYPES.SEARCH_QUERY_CHANGED;

	constructor(public payload: string) {}
}

export class LoadSearchResults implements Action {
	readonly type = SEARCH_ACTION_TYPES.LOAD_SEARCH_RESULTS.REQUESTED;
}

export class ClearSearchResults implements Action {
	readonly type = SEARCH_ACTION_TYPES.CLEAR_SEARCH_RESULTS;
}
