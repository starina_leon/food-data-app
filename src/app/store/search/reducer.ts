import {SEARCH_ACTION_TYPES} from './actions';

export interface SearchState {
	searchQuery: string;
	searchResult: any;
}

const initialSearchState: SearchState = {
	searchQuery: '',
	searchResult: []
};

export function searchReducer(state = initialSearchState, action) {
	switch (action.type) {
		case SEARCH_ACTION_TYPES.SEARCH_QUERY_CHANGED:
			return {
				...state,
				searchQuery: action.payload
			};
		case SEARCH_ACTION_TYPES.LOAD_SEARCH_RESULTS.SUCCEEDED:
			return {
				...state,
				searchResult: action.payload
			};
		case SEARCH_ACTION_TYPES.CLEAR_SEARCH_RESULTS:
			return {
				...state,
				searchResult: initialSearchState.searchResult
			};
		default:
			return state;
	}
}
