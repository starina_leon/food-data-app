import {Action} from '@ngrx/store';

const moduleName = 'Dashboard';

export const DASHBOARD_ACTION_TYPES = {
	SELECT_DATE: `[${moduleName}] Select date`,
	ADD_DETAILS_CLICKED: `[${moduleName}] Food Item Added`,
	SAVE_FROM_DETAILS: `[${moduleName}] Save from Details`,
	SAVE_FROM_SEARCH: `[${moduleName}] Save from Search`,
	ADD_FROM_SEARCH_CLICKED: `[${moduleName}] Add from Search Clicked`,
	DELETE_FOOD_ITEM: `[${moduleName}] Delete Food Item`,
	SET_WEIGHT: `[${moduleName}] Set Weight`,
	OPEN_FOOD_ITEM_EDIT_DIALOG: `[${moduleName}] Open food item edit Dialog`
};

export class SelectDate implements Action {
	readonly type = DASHBOARD_ACTION_TYPES.SELECT_DATE;
	constructor(public payload: {date: string}) {}
}

export class ClickOnAddDetails implements Action {
	readonly type = DASHBOARD_ACTION_TYPES.ADD_DETAILS_CLICKED;
}

export class SaveFromDetails implements Action {
	readonly type = DASHBOARD_ACTION_TYPES.SAVE_FROM_DETAILS;

	constructor(public payload: {date: string; food: number; group: string}) {}
}

export class SaveFromSearch implements Action {
	readonly type = DASHBOARD_ACTION_TYPES.SAVE_FROM_SEARCH;
	constructor(public payload: {date: string; food: number; group: string}) {}
}

export class AddFromSearchClicked implements Action {
	readonly type = DASHBOARD_ACTION_TYPES.ADD_FROM_SEARCH_CLICKED;
	constructor(public payload: {food: any}) {}
}

export class DeleteFoodItem implements Action {
	readonly type = DASHBOARD_ACTION_TYPES.DELETE_FOOD_ITEM;
	constructor(public payload: {id: number; group: string}) {}
}

export class SetWeight implements Action {
	readonly type = DASHBOARD_ACTION_TYPES.SET_WEIGHT;
	constructor(public payload: {id: number; weight: number; group: string}) {}
}

export class OpenFoodItemEditDialog implements Action {
	readonly type = DASHBOARD_ACTION_TYPES.OPEN_FOOD_ITEM_EDIT_DIALOG;
	constructor(public payload: {food: any; group: string}) {}
}
