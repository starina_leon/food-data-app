import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Action, Store} from '@ngrx/store';
import {AppState} from '../root.reducer';
import {Observable} from 'rxjs';
import {
	AddFromSearchClicked,
	DASHBOARD_ACTION_TYPES,
	OpenFoodItemEditDialog,
	SaveFromDetails,
	SaveFromSearch,
	SetWeight
} from './actions';
import {map, tap, withLatestFrom} from 'rxjs/operators';
import {getDetailsInfo} from '../details/selectors';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';
import {DialogPageContainer} from '../../pages/dashboard/dialog-page/dialog-page.container';

@Injectable()
export class DashboardEffects {
	constructor(
		private action$: Actions,
		private store: Store<AppState>,
		private snackbar: MatSnackBar,
		private dialog: MatDialog
	) {}

	@Effect({dispatch: false})
	saveDetails$: Observable<[Action, any]> = this.action$.pipe(
		ofType(DASHBOARD_ACTION_TYPES.ADD_DETAILS_CLICKED),
		withLatestFrom(this.store.select(getDetailsInfo)),
		tap(([_, detailsInfo]) => {
			const details = {
				date: localStorage.getItem('date'),
				food: detailsInfo,
				group: localStorage.getItem('group'),
				weight: null
			};
			this.dialog
				.open(DialogPageContainer, {
					data: {
						hideRemoveButton: true
					}
				})
				.afterClosed()
				.subscribe((result) => {
					if (result) {
						details.weight = result.weight;
						this.store.dispatch(new SaveFromDetails(details));
					}
				});
		})
	);

	@Effect({dispatch: false})
	showMessage$: Observable<Action> = this.action$.pipe(
		ofType(DASHBOARD_ACTION_TYPES.SAVE_FROM_SEARCH, DASHBOARD_ACTION_TYPES.SAVE_FROM_DETAILS),
		tap(() => {
			this.snackbar.open('Food is added', 'Ok', {duration: 2000});
		})
	);

	@Effect({dispatch: false})
	addFromSearch$: Observable<Action> = this.action$.pipe(
		ofType(DASHBOARD_ACTION_TYPES.ADD_FROM_SEARCH_CLICKED),
		tap((_: AddFromSearchClicked) => {
			const details = {
				date: localStorage.getItem('date'),
				food: _.payload.food,
				group: localStorage.getItem('group'),
				weight: null
			};
			this.dialog
				.open(DialogPageContainer, {
					data: {
						hideRemoveButton: true
					}
				})
				.afterClosed()
				.subscribe((result) => {
					if (result) {
						details.weight = result.weight;
						this.store.dispatch(new SaveFromSearch(details));
					}
				});
		})
	);

	@Effect({dispatch: false})
	openDialog$: Observable<Action> = this.action$.pipe(
		ofType(DASHBOARD_ACTION_TYPES.OPEN_FOOD_ITEM_EDIT_DIALOG),
		tap((_: OpenFoodItemEditDialog) => {
			const foodItem = {
				food: _.payload.food,
				group: _.payload.group
			};
			this.dialog
				.open(DialogPageContainer, {
					data: foodItem
				})
				.afterClosed()
				.subscribe((result) => {
					if (result) {
						this.store.dispatch(
							new SetWeight({
								id: foodItem.food.id,
								group: foodItem.group,
								weight: result.weight
							})
						);
					}
				});
		})
	);
}
