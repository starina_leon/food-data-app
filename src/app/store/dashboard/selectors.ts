import {AppState} from '../root.reducer';
import {createSelector} from '@ngrx/store';
import {getSettingsState} from '../settings/selectors';

export const getDashboardState = (state: AppState) => state.dashboard;

export const getDashboardDateMeals = createSelector(
	getDashboardState,
	(state) => {
		return state.mealsByDates[state.currentDate] || ({} as any);
	}
);

export const getDashboardDateSums = createSelector(
	getDashboardState,
	(state) => {
		const progress = {
			carbs: 0,
			protein: 0,
			fats: 0,
			calories: 0
		};
		if (!state.mealsByDates[state.currentDate]) {
			return progress;
		}
		const data = Object.keys(state.mealsByDates[state.currentDate]).reduce((res, groupKey) => {
			return state.mealsByDates[state.currentDate][groupKey].reduce((groupRes, food) => {
				groupRes.carbs += food.carbs;
				groupRes.protein += food.protein;
				groupRes.fats += food.fats;
				// groupRes.calories += food.calories;
				return groupRes;
			}, res);
		}, progress);

		return data;
	}
);

export const getDashboardProgress = createSelector(
	getDashboardDateSums,
	getSettingsState,
	(sums, settings) => {
		const progress = {
			carbs: Math.round((sums.carbs / +settings.carbs) * 100),
			protein: Math.round((sums.protein / +settings.proteins) * 100),
			fats: Math.round((sums.fats / +settings.fats) * 100)
			// calories: Math.round((sums.calories / +settings.calories * 100))
		};

		if (!Number.isFinite(progress.carbs)) {
			progress.carbs = 0;
		}
		if (!Number.isFinite(progress.protein)) {
			progress.protein = 0;
		}

		if (!Number.isFinite(progress.fats)) {
			progress.fats = 0;
		}

		return progress;
	}
);

export const getDashboardWeights = createSelector(
	getDashboardDateMeals,
	getDashboardDateSums,
	(state, data) => {
		const weights = {
			carbs: 0,
			protein: 0,
			fats: 0
			// calories: 0
		};
		console.log(state, data);
		return state;
	}
);
