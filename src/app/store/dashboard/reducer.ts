import {DASHBOARD_ACTION_TYPES} from './actions';
import {DATE_FORMAT_URL} from '../../shared/constants';
import * as moment from 'moment';

export interface FoodItem {
	name;
	protein: number;
	carbs: number;
	fats: number;
	calories?: '';
	id: number;
	group: string;
	weight: number;
}

export interface DashboardDayState {
	breakfast: FoodItem[];
	lunch: FoodItem[];
	dinner: FoodItem[];
	snacks: FoodItem[];
}

export interface DashboardState {
	currentDate: string;
	mealsByDates: {
		[date: string]: DashboardDayState;
	};
}

const initialDashboardState: DashboardState = {
	currentDate: localStorage.getItem('date') || moment().format(DATE_FORMAT_URL),
	mealsByDates: {
		'02-06-2020': {
			breakfast: [
				{
					name: 'Eggs, Grade A, Large, egg white',
					protein: 10.7,
					carbs: 2.36,
					fats: 0,
					calories: '',
					id: 747997,
					group: 'breakfast',
					weight: null
				}
			],
			lunch: [],
			dinner: [],
			snacks: []
		}
	}
};

export function dashboardReducer(state = initialDashboardState, action) {
	switch (action.type) {
		case DASHBOARD_ACTION_TYPES.SELECT_DATE:
			return {
				...state,
				currentDate: action.payload.date
			};
		case DASHBOARD_ACTION_TYPES.SAVE_FROM_SEARCH:
			return saveFoodItemsFromSearch(state, action);

		case DASHBOARD_ACTION_TYPES.SAVE_FROM_DETAILS:
			return saveFoodItemsFromDetail(state, action);

		case DASHBOARD_ACTION_TYPES.DELETE_FOOD_ITEM:
			return deleteFoodItemFromStore(state, action);

		case DASHBOARD_ACTION_TYPES.SET_WEIGHT:
			return setWeightToStore(state, action);

		default:
			return state;
	}
}

function deleteFoodItemFromStore(state, action) {
	const date = localStorage.getItem('date');
	const foodGroup = action.payload.group;
	state = JSON.parse(JSON.stringify(state));
	state.mealsByDates[date][foodGroup] = state.mealsByDates[date][foodGroup].filter(
		(foodItem) => foodItem.id !== action.payload.id
	);
	console.log(state, action);
	return state;
}

function setWeightToStore(state, action) {
	const date = localStorage.getItem('date');
	const group = action.payload.group;
	state = JSON.parse(JSON.stringify(state));
	state.mealsByDates[date][group].find((foodItem) => foodItem.id === action.payload.id).weight =
		action.payload.weight;
	return state;
}

function saveFoodItemsFromSearch(state: DashboardState, action) {
	let mealsByGroups = state.mealsByDates[action.payload.date];

	if (!mealsByGroups) {
		mealsByGroups = {
			breakfast: [],
			lunch: [],
			dinner: [],
			snacks: []
		};
	}

	const protein = action.payload.food.foodNutrients.find((foodNutrients) => foodNutrients.nutrientName === 'Protein');
	const fats = action.payload.food.foodNutrients.find(
		(foodNutrients) => foodNutrients.nutrientName === 'Total lipid (fat)'
	);
	const carbs = action.payload.food.foodNutrients.find(
		(foodNutrients) => foodNutrients.nutrientName === 'Carbohydrate, by difference'
	);
	const name = action.payload.food.description;
	const id = action.payload.food.fdcId;
	const group = action.payload.group;
	const weight = action.payload.weight;

	const nutrients = {
		name,
		protein: protein.value,
		carbs: carbs.value,
		fats: fats.value,
		calories: '',
		id,
		group,
		weight
	};

	mealsByGroups = JSON.parse(JSON.stringify(mealsByGroups));

	mealsByGroups[action.payload.group].push(nutrients);

	return {
		currentDate: state.currentDate,
		mealsByDates: {
			...state.mealsByDates,
			[action.payload.date]: mealsByGroups
		}
	};
}

function saveFoodItemsFromDetail(state: DashboardState, action) {
	let mealsByGroups = state.mealsByDates[action.payload.date];

	if (!mealsByGroups) {
		mealsByGroups = {
			breakfast: [],
			lunch: [],
			dinner: [],
			snacks: []
		};
	}

	const protein = action.payload.food.foodNutrients.find(
		(foodNutrients) => foodNutrients.nutrient.name === 'Protein'
	);
	const fats = action.payload.food.foodNutrients.find(
		(foodNutrients) => foodNutrients.nutrient.name === 'Total lipid (fat)'
	);
	const carbs = action.payload.food.foodNutrients.find(
		(foodNutrients) => foodNutrients.nutrient.name === 'Carbohydrate, by difference'
	);
	const name = action.payload.food.description;
	const id = action.payload.food.fdcId;
	const group = action.payload.group;
	const weight = action.payload.weight;

	const nutrients = {
		name,
		protein: protein.amount,
		carbs: carbs.amount,
		fats: fats.amount,
		calories: '',
		id,
		group,
		weight
	};

	mealsByGroups = JSON.parse(JSON.stringify(mealsByGroups));

	mealsByGroups[action.payload.group].push(nutrients);

	return {
		currentDate: state.currentDate,
		mealsByDates: {
			...state.mealsByDates,
			[action.payload.date]: mealsByGroups
		}
	};
}
