import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SearchModule} from './pages/food-search/search.module';
import {DashboardModule} from './pages/dashboard/dashboard.module';
import {ToolbarComponent} from './shared/components/toolbar/toolbar.component';
import {AppRoutingModule} from './app-routing.module';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';

import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {environment} from '../environments/environment';
import {reducers} from './store/root.reducer';
import {SearchEffects} from './store/search/effects';
import {DetailsEffects} from './store/details/effects';
import {ToolbarContainer} from './shared/components/toolbar/toolbar.container';
import {DashboardEffects} from './store/dashboard/effects';
import {MatSnackBar, MatSnackBarModule} from '@angular/material/snack-bar';

@NgModule({
	declarations: [AppComponent, ToolbarComponent, ToolbarContainer],
	imports: [
		AppRoutingModule,
		BrowserModule,
		SearchModule,
		DashboardModule,
		FormsModule,
		HttpClientModule,
		ReactiveFormsModule,
		BrowserAnimationsModule,
		MatToolbarModule,
		MatIconModule,
		MatButtonModule,
		MatSnackBarModule,
		StoreModule.forRoot(reducers),
		EffectsModule.forRoot([SearchEffects, DetailsEffects, DashboardEffects]),
		StoreDevtoolsModule.instrument({
			name: 'Food Data App',
			maxAge: 25,
			logOnly: environment.production
		})
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule {}
