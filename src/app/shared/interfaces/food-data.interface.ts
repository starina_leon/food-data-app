export interface FoodDataInterface {
	calories: number;
	carbs: number;
	fats: number;
	protein: number;
}

export interface DialogData {
	hideRemoveButton: boolean;
	food: any;
	group: string;
}

export interface Progress {
	calories: number;
	carbs: number;
	fats: number;
	protein: number;
}
