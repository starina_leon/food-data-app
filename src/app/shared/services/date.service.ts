import {Injectable} from '@angular/core';
import * as moment from 'moment';
import {DATE_FORMAT_URL} from '../constants';
import {BehaviorSubject} from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class DateService {
	date = new BehaviorSubject<moment.Moment>(null);

	constructor() {
		if (!localStorage.getItem('date')) {
			localStorage.setItem('date', moment().format(DATE_FORMAT_URL));
		}
		const date = moment(localStorage.getItem('date'), DATE_FORMAT_URL);
		this.date.next(date);
	}

	toNextDate() {
		this.date.next(this.date.value.add(1, 'day'));
		localStorage.setItem('date', moment(this.date.value).format(DATE_FORMAT_URL));
	}

	toPreviousDate() {
		this.date.next(this.date.value.subtract(1, 'day'));
		localStorage.setItem('date', moment(this.date.value).format(DATE_FORMAT_URL));
	}
}
