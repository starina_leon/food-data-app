import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
	selector: 'app-toolbar',
	templateUrl: './toolbar.component.html',
	styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {
	@Output() foodItemClicked = new EventEmitter();

	constructor() {}

	ngOnInit(): void {}

	public get isButtonVisible(): boolean {
		return !!window.location.href.match(/details\/\d+/);
	}

	addFoodItem() {
		this.foodItemClicked.emit();
	}
}
