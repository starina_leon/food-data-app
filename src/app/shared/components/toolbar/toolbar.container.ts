import {Component} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../../../store/root.reducer';
import {ClickOnAddDetails} from '../../../store/dashboard/actions';

@Component({
	selector: 'app-toolbar-container',
	template: '<app-toolbar  (foodItemClicked)="clickOnAddDetails()"></app-toolbar>'
})
export class ToolbarContainer {
	constructor(private store: Store<AppState>) {}

	clickOnAddDetails() {
		this.store.dispatch(new ClickOnAddDetails());
	}
}
