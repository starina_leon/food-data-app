/**
 * Returns a random integer between min (inclusive) and max (inclusive).
 * The value is no lower than min (or the next integer greater than min
 * if min isn't an integer) and no greater than max (or the next integer
 * lower than max if max isn't an integer).
 * Using Math.round() will give you a non-uniform distribution!
 */
function getRandomInt(min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getSingleFoodData() {
	const names = ['steak', 'celery', 'soup', 'egg', 'french fries', 'butter', 'cereal', 'burger'];
	return {
		name: names[getRandomInt(0, names.length - 1)],
		description: 'Coming Soon',
		data: {
			weight: getRandomInt(50, 200),
			calories: getRandomInt(100, 300),
			fat: getRandomInt(1, 25),
			protein: getRandomInt(10, 50),
			carbs: getRandomInt(30, 80)
		}
	};
}

export function getMeal() {
	return ['Breakfast', 'Lunch', 'Dinner', 'Snacks'].map((mealName) => ({
		group: mealName,
		total: {
			calories: getRandomInt(100, 300),
			fat: getRandomInt(1, 25),
			protein: getRandomInt(10, 50),
			carbs: getRandomInt(30, 80)
		},
		foods: Array.from(Array(getRandomInt(1, 8))).map((i) => getSingleFoodData())
	}));
}

export function getURL(food) {
	return `https://fdc.nal.usda.gov/fdc-app.html#/food-details/${food.fdcId}/nutrients`;
}
